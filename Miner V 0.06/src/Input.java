import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Input {

    static int getInputInt() throws IOException {
        int inputInt = 0;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String input = in.readLine();
        if (isInteger(input)) {
            inputInt = Integer.parseInt(input);
        } else {
            System.out.println("Error. Input only numbers.");
        }
        return inputInt;
    }

    private static boolean isInteger(String inputStr) {
        try {
            Integer.valueOf(inputStr);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
