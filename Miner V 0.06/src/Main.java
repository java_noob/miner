import java.io.IOException;


public class Main {

    public static void main(String args[]) throws IOException {
        System.out.println("Hello!");
        int ex = 0;
        while (ex == 0) {
            System.out.println("");
            System.out.println("Menu:");
            System.out.println("1)Play");
            System.out.println("2)Setting");
            System.out.println("3)Info");
            System.out.println("4)Exit");
            int m = Input.getInputInt();
            switch (m) {
                case 1:
                    Play.Game();
                    System.out.println("Continue?");
                    System.out.println("(1)Yes : (2)No");
                    int e = Input.getInputInt();
                    if (e == 1) {
                        break;
                    } else {
                        ex = 1;
                        System.out.println("Bye =)");
                        break;
                    }
                case 2:
                    System.out.println("Setting:");
                    System.out.println("Enter 1 - to easy;");
                    System.out.println("Enter 2 - to normal;");
                    System.out.println("Enter 3 - to hard;");
                    System.out.println("Enter 4 - to choose size and mine count");
                    int dif = Input.getInputInt();
                    if (dif < 4 && dif > 0) {
                        System.out.println(Setting.changeDif(dif));
                    } else if (dif == 4) {
                        System.out.println("Length: ");
                        int x = Input.getInputInt();
                        if (x < 1 || x > 50) {
                            System.out.println("Error. Please reenter.");
                            break;
                        }
                        System.out.println("Height: ");
                        int y = Input.getInputInt();
                        if (y < 1 || y > 50) {
                            System.out.println("Error. Please reenter.");
                            break;
                        }
                        System.out.println("Mines: ");
                        int c = Input.getInputInt();
                        if (c < 1 || c > x * y - 1) {
                            System.out.println("Error. Please reenter.");
                            break;
                        }
                        System.out.println(Setting.changeParameters(x, y, c));
                    } else {
                        System.out.println("Wrong choice");
                    }
                    break;
                case 3:
                    System.out.println("Miner V0.05");
                    System.out.println("(1) Rule : (2) Info");
                    int i = Input.getInputInt();
                    switch (i){
                        case 1:
                            System.out.println("Miner");
                            System.out.println("1) You must find all mine and open all cell");
                            System.out.println("2) In cell showed count mine around chosen cell");
                            System.out.println("3) Double click on cell activate helper:");
                            System.out.println("If around cell all mine founded right - opened another cell around who closed");
                            System.out.println("Game over if mine not found or you make mistake");
                            System.out.println("4) You can create you field with many variant of size and mine count");
                            System.out.println("Be careful and good luck =)");
                            break;
                        case 2:
                            System.out.println("Made by Exor4eG");
                            System.out.println("Creating for fun and practical");
                            System.out.println("Send to my mail you mind about this");
                            System.out.println("Exor4eg@gmail.com");
                            System.out.println("______________$$_____________________ $$");
                            System.out.println("____________$$$__$__________________$__$$$");
                            System.out.println("___________$$$___$$________________$$___$$$");
                            System.out.println("___________$$$$$$$$________________$$$$$$$$");
                            System.out.println("____________$$$$$$__________________$$$$$$");
                            System.out.println("_____________$$$$____$$0$$$$$0$$$____$$$$");
                            System.out.println("_______________$$__$$$$$$$$$$$$$$$$__$$");
                            System.out.println("___________$$___$$$$$$$$$$$$$$$$$$$$$$___$$");
                            System.out.println("_________$$__$$__$$$$$$$$$$$$$$$$$$$$__$$__$$");
                            System.out.println("________$______$$$$$$$$$$$$$$$$$$$$$$$$______$");
                            System.out.println("________$__$$$____$$$$$$$$$$$$$$$$$$____$$$__$");
                            System.out.println("__________$___$$$$_$$$$$$$$$$$$$$$$_$$$$___$");
                            System.out.println("_________$_________$_$$$$$$$$$$$$_$_________$");
                            System.out.println("_________$______$$$________________$$$______$");
                            System.out.println("_______________$______________________$");
                            System.out.println("______________$________________________$");
                            System.out.println("______________$_______________________ _$");
                    }
                    break;
                case 4:
                    System.out.println("Exit");
                    System.out.println("Bye =)");
                    System.exit(1);
                default:
                    System.out.println("Wrong choice");
            }
        }
    }
}