class Setting {
    private static int countMine = 10;
    private static int length = 5;
    private static int height = 10;
    private static String difficult;

    static String changeDif(int dif) {
        if (dif < 1 || dif > 3) {
            difficult = "Wrong LVL!";
        }
        if (dif == 1) {
            length = 5;
            height = 10;
            countMine = 10;
            difficult = "You choose easy lvl";
        }
        if (dif == 2) {
            length = 10;
            height = 20;
            countMine = 50;
            difficult = "You choose medium lvl";
        }
        if (dif == 3) {
            length = 15;
            height = 25;
            countMine = 125;
            difficult = "You choose hard lvl";
        }
        return difficult;
    }

    static String changeParameters(int x, int y, int c) {
        length = x;
        height = y;
        countMine = c;
        difficult = "You field " + x + ":" + y + ". And has " + c + " mine";
        return difficult;
    }

    static int getCountMine() {
        return countMine;
    }

    static int getLength() {
        return length;
    }

    static int getHeight() {
        return height;
    }
}
