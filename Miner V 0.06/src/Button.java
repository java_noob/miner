class Button {
    int state;
    int data;
    String show;

    Button() {
        state = 0;
        data = 0;
        show = "O";
    }

    public int getState() {
        return state;
    }

    void setState(int state) {
        this.state = state;
    }

    int getData() {
        return data;
    }

    void setData(int data) {
        this.data = data;
    }

    String getShow() {
        return show;
    }

    void setShow(String show) {
        this.show = show;
    }
}