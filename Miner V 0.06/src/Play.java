import java.io.IOException;

class Play {
    static void Game() throws IOException {
        Field.initializeField();
        Field.showArr();
        int i = 1;
        while (i == 1) {
            System.out.println("Enter x=");
            int a = Input.getInputInt();
            int x = a - 1;
            if (a > Setting.getLength() || a < 1) {
                System.out.println("Wrong choice");
                continue;
            }
            System.out.println("Enter y=");
            int b = Input.getInputInt();
            int y = b - 1;
            if (b > Setting.getHeight() || b < 1) {
                System.out.println("Wrong choice");
                continue;
            }
            System.out.println("You choose " + a + ":" + b);
            System.out.println("To open - press 1");
            System.out.println("To marker mine - press 2");
            System.out.println("To marker - press 3");
            System.out.println("To re-elected - press 4");
            System.out.println("To exit to menu - press 5");
            int choice = Input.getInputInt();
            switch (choice) {
                case 1:
                    i = GameAction.OpenCell(x, y);
                    if (i == 0) {
                        System.out.println("Game Over");
                    }
                    break;
                case 2:
                    GameAction.SetMine(x, y);
                    break;
                case 3:
                    GameAction.SetWhat(x, y);
                case 4:
                    break;
                case 5:
                    i = 0;
                    break;
                default:
                    System.out.println("Wrong choice");
                    break;
            }
            if (GameAction.Win()) {
                System.out.println("You Win!");
                System.out.println("Congratulation`s");
                i = 0;
            }
        }
    }
}
