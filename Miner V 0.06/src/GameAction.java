class GameAction {
    static int OpenCell(int x, int y) {
        if (Field.Arr[x][y].getState() == 1) {
            return Check(x, y);
        } else if (Field.Arr[x][y].getData() != 10) {
            Open(x, y);
            Field.showArr();
            return 1;
        } else {
            Lose();
            Field.showArr();
            return 0;
        }
    }

    private static void Open(int x, int y) {
        Field.Arr[x][y].setState(1);
        Field.Arr[x][y].setShow("" + Field.Arr[x][y].getData());
        if (Field.Arr[x][y].getData() == 0) {
            OpenZero(x, y);
        }
    }

    private static int Check(int x, int y) {
        int mineOpen = 0;
        int mineMarked = 0;
        for (int y1 = y - 1; y1 < y + 2; y1++) {
            for (int x1 = x - 1; x1 < x + 2; x1++) {
                if (x1 >= 0 && y1 >= 0 && y1 < Setting.getHeight() && x1 < Setting.getLength()) {
                    if (Field.Arr[x1][y1].getShow().equals("M")) {
                        mineMarked++;
                    }
                    if (Field.Arr[x1][y1].getData() == 10) {
                        mineOpen++;
                    }
                }
            }
        }
        if (mineMarked == mineOpen) {
            for (int y1 = y - 1; y1 < y + 2; y1++) {
                for (int x1 = x - 1; x1 < x + 2; x1++) {
                    if (y1 >= 0 && x1 >= 0 && y1 < Setting.getHeight() && x1 < Setting.getLength()) {
                        if (Field.Arr[x1][y1].getData() != 10) {
                            Open(x1, y1);
                        }
                    }
                }
            }
            Field.showArr();
            return 1;
        } else {
            Lose();
            Field.showArr();
            return 0;
        }
    }

    static void SetMine(int x, int y) {
        Field.Arr[x][y].setShow("M");
        Field.showArr();
    }

    static void SetWhat(int x, int y) {
        Field.Arr[x][y].setShow("?");
        Field.showArr();
    }

    private static void OpenZero(int x, int y) {
        for (int y1 = y - 1; y1 < y + 2; y1++) {
            for (int x1 = x - 1; x1 < x + 2; x1++) {
                if (y1 >= 0 && x1 >= 0 && y1 < Setting.getHeight() && x1 < Setting.getLength()) {
                    if (Field.Arr[x1][y1].getData() == 0 && Field.Arr[x1][y1].getState() == 0) {
                        OpenCell(x1, y1);
                    } else {
                        Field.Arr[x1][y1].setShow("" + Field.Arr[x1][y1].getData());
                        Field.Arr[x1][y1].setState(1);
                    }
                }
            }
        }
    }

    static Boolean Win() {
        int offState = 0;
        for (int i = 0; i < Setting.getHeight(); i++) {
            for (int e = 0; e < Setting.getLength(); e++) {
                if (Field.Arr[e][i].getState() == 0) {
                    offState++;
                }
            }
        }
        return Setting.getCountMine() == offState;
    }

    private static void Lose() {
        for (int b = 0; b < Setting.getHeight(); b++) {
            for (int a = 0; a < Setting.getLength(); a++) {
                if (Field.Arr[a][b].getData() == 10) {
                    Field.Arr[a][b].setShow("X");
                }
            }
        }
    }
}
