class Field {
    static Button[][] Arr;
    private static int length;
    private static int height;

    //Initialize field + input data for button with mine
    static void initializeField() {
        length = Setting.getLength();
        height = Setting.getHeight();
        int countMine = Setting.getCountMine();
        Arr = new Button[length][height];
        for (int m = 0; m < height; m++) {
            for (int n = 0; n < length; n++) {
                Arr[n][m] = new Button();
            }
        }
        for (int i = 0; i < countMine; i++) {
            int n = (int) (height * Math.random());
            int m = (int) (length * Math.random());
            if (Arr[m][n].getData() == 0) {
                Arr[m][n].setData(10);
            } else {
                i--;
            }
        }
//input data to another button without mine
        for (int m = 0; m < height; m++) {
            for (int n = 0; n < length; n++) {
                if (Arr[n][m].getData() != 10) {
                    int data = 0;
                    for (int h = m - 1; h < m + 2; h++) {
                        for (int l = n - 1; l < n + 2; l++) {
                            if (l >= 0 && h >= 0 && l < length && h < height) {
                                if (Arr[l][h].getData() == 10) {
                                    data++;
                                }
                            }
                        }
                    }
                    Arr[n][m].setData(data);
                }
            }
        }
    }

    static void showArr() {
        for (int n = 0; n < length; n++) {
            if (n > 8) {
                System.out.print(n + 1 + " ");
            } else {
                System.out.print(n + 1 + "  ");
            }
        }
        System.out.println("");
        for (int n = 0; n < length * 3 - 2; n++) {
            System.out.print("_");
        }
        for (int m = 0; m < height; m++) {
            System.out.println("");
            for (int n = 0; n < length; n++) {
                System.out.print(Arr[n][m].getShow() + "  ");
            }
            System.out.print("|  " + (m + 1));
        }
        System.out.println("");
        for (int n = 0; n < length * 3 - 2; n++) {
            System.out.print("_");
        }
        System.out.println("");
        for (int n = 0; n < length; n++) {
            if (n > 8) {
                System.out.print(n + 1 + " ");
            } else {
                System.out.print(n + 1 + "  ");
            }
        }
        System.out.println("");
    }
}
